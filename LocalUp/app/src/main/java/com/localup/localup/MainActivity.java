package com.localup.localup;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.Socket;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final int CODE = 200;

    private Button b;
    private EditText t;
    private Intent i;

    private static final int PORT = 1270;
    private static final int DIM = 1024;

    private BufferedInputStream bis;
    private BufferedOutputStream bos;
    private Socket s;
    private byte[] buffer = new byte[DIM];

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b = (Button) findViewById(R.id.button);
        t = (EditText) findViewById(R.id.ip);
        i = new Intent(Intent.ACTION_GET_CONTENT);
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        i.setType("*/*");

        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.button: startActivityForResult(i, CODE); break;
            default: break;
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE)
        {
            if (data != null)
            {
                if (data.getClipData() != null)
                {
                    for(int i = 0; i < data.getClipData().getItemCount(); i++)
                    {
                        new Thread(new BackgroundThread(
                                getContentResolver(),
                                data.getClipData().getItemAt(i).getUri(),
                                t.getText().toString()
                        )).start();
                    }
                }
                else
                {
                    new Thread(new BackgroundThread(
                            getContentResolver(),
                            data.getData(),
                            t.getText().toString()
                    )).start();
                }
            }
        }
    }
}