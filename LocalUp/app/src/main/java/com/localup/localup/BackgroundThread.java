package com.localup.localup;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static android.content.ContentValues.TAG;

class BackgroundThread implements Runnable
{
    private static final int PORT = 1270;
    private static final int DIM = 1024;

    private InputStream is;
    private BufferedOutputStream bos;
    private Socket s;
    private byte[] buffer = new byte[DIM];

    private ContentResolver cr;
    private Uri file;
    private String ip;

    public BackgroundThread(ContentResolver cr, Uri file, String ip)
    {
        this.cr = cr;
        this.file = file;
        this.ip = ip;
    }

    @Override
    public void run()
    {
        try
        {
            Cursor c = cr.query(file, null, null, null, null);
            c.moveToFirst();
            int nameIndex = c.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            int sizeIndex = c.getColumnIndex(OpenableColumns.SIZE);

            is = cr.openInputStream(file);
            s = new Socket(InetAddress.getByName(ip), PORT);
            bos = new BufferedOutputStream(s.getOutputStream());

            //send name
            //Log.d(TAG, "send name -> " + c.getString(nameIndex));
            Arrays.fill(buffer, (byte) 0);
            byte[] bb = c.getString(nameIndex).getBytes();
            for (int i = 0; i < bb.length; i++)
            {
                buffer[i] = bb[i];
            }
            bos.write(buffer, 0, buffer.length);

            //send size
            //Log.d(TAG, "send size -> " + c.getLong(sizeIndex));
            bb = ByteBuffer.allocate(Long.BYTES).putLong(c.getLong(sizeIndex)).array();
            bos.write(bb, 0, bb.length);

            //send file
            //Log.d(TAG, "send file");
            Arrays.fill(buffer, (byte) 0);
            int bytesRead;
            while ((bytesRead = is.read(buffer, 0, buffer.length)) > 0)
            {
                //Log.d(TAG, Integer.toString(bytesRead));
                bos.write(buffer, 0, bytesRead);
            }

            bos.flush();
            is.close();
            bos.close();
            s.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}