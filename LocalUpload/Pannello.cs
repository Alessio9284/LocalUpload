﻿using System;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace LocalUpload
{
    public partial class frame : MetroForm
    {
        public static ProgressBar pb;
        public static Label l;
        public static FlowLayoutPanel p;
        public static Label ip;

        private static Thread t;

        public frame()
        {
            InitializeComponent();

            pb = progress;
            l = label1;
            p = panel;
            ip = label2;
        }

        private void frame_Load(object sender, EventArgs e)
        {
            ip.Text = Dns.GetHostEntry(Dns.GetHostName()).AddressList[Dns.GetHostEntry(Dns.GetHostName()).AddressList.Length - 1].ToString();
            t = new Thread(new luserver().start);
            t.IsBackground = true;
            t.Start();
        }
    }
}
