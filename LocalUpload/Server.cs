﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Drawing;

namespace LocalUpload
{
    class luserver
    {
        private const int PORT = 1270;
        private const int DIM = 1024;
        private const int LONG = 8;
        private byte[] buffer = new byte[DIM];
        private byte[] byte_size = new byte[LONG];
        private string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        private string[] mult_byte = { "Byte", "KB", "MB", "GB" };

        private string file_name;
        private long file_size;
        private int packets;

        public void start()
        {
            TcpListener server = new TcpListener(IPAddress.Any, PORT);
            server.Start();

            while (true)
            {
                using (TcpClient client = server.AcceptTcpClient())
                using (NetworkStream stream = client.GetStream())
                {
                    Array.Clear(buffer, 0, DIM);
                    stream.Read(buffer, 0, DIM);
                    file_name = Regex.Replace(Encoding.UTF8.GetString(buffer).TrimEnd('\0'), "[^A-Za-z0-9_. ]+", "");
                    //Console.WriteLine(file_name);

                    Array.Clear(byte_size, 0, LONG);

                    stream.Read(byte_size, 0, LONG);
                    Array.Reverse(byte_size, 0, LONG);
                    file_size = BitConverter.ToInt64(byte_size, 0);
                    //Console.WriteLine(file_size);

                    Array.Clear(buffer, 0, buffer.Length);

                    packets = (int) Math.Ceiling(file_size / 1024.0);

                    frame.l.Invoke((MethodInvoker)delegate
                    {
                        frame.l.Text = desktop + "\\" + file_name;
                    });

                    using (var output = File.Create(desktop + "\\" + file_name))
                    {
                        int bytesRead, packet = 0;
                        while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            output.Write(buffer, 0, bytesRead);
                            packet = (packet == packets ? packet : ++packet);

                            if(packets > 0)
                            {
                                int temp = (packet * 100) / packets;

                                //Console.WriteLine("packet: " + packet + "; packets: " + packets + "; temp: " + temp);

                                frame.pb.Invoke((MethodInvoker)delegate
                                {
                                    frame.pb.Value = (temp > 0 ? temp : 0);
                                });
                            }
                        }
                    }

                    //Console.WriteLine("aggiunta riga");
                    frame.p.Invoke((MethodInvoker)delegate
                    {
                        Panel p = new Panel();
                        p.BackColor = Color.WhiteSmoke;

                        p.Controls.Add(new PictureBox()
                        {
                            Image = Icon.ExtractAssociatedIcon(desktop + "\\" + file_name).ToBitmap(),
                            SizeMode = PictureBoxSizeMode.CenterImage,
                            BackgroundImageLayout = ImageLayout.Center,
                            Size = new Size(50, 50),
                            Location = new Point(p.Width/2 - 25, 3),
                        });
                        p.Controls.Add(new Label()
                        {
                            Text = file_name,
                            Location = new Point(3, 56),
                            TextAlign = ContentAlignment.MiddleCenter,
                            Width = p.Width - 3
                        });
                        p.Controls.Add(new Label()
                        {
                            Text = convertByte(file_size),
                            Location = new Point(3, 78),
                            TextAlign = ContentAlignment.MiddleCenter,
                            Width = p.Width - 3
                        });

                        frame.p.Controls.Add(p);
                    });

                    frame.l.Invoke((MethodInvoker)delegate
                    {
                        frame.l.Text = "";
                    });

                    frame.pb.Invoke((MethodInvoker)delegate
                    {
                        frame.pb.Value = 0;
                    });
                }
            }
        }

        private string convertByte(long bytes)
        {
            var i = 0;
            string result;

            for (;;)
            {
                if (bytes < 1024)
                {
                    result = bytes + " " + mult_byte[i];
                    break;
                }

                bytes /= 1024;
                i++;

                if(i == 4)
                {
                    result = "Too Large";
                    break;
                }
            }

            return result;
        }
    }
}
